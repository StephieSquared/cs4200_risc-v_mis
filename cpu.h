/*
 * Copyright (C) 2018 Gedare Bloom
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdint.h>

struct cpu_context {
	uint32_t PC;
	uint32_t GPR[32];
	uint8_t sys_exit;
	uint8_t spinDown;
};

extern struct cpu_context cpu_ctx;

struct IF_ID_buffer {
	uint32_t instruction;
	uint32_t pc;
	uint32_t next_pc;
};

struct ID_EX_buffer {
	// control singnals
	uint8_t CtrlMemRead;
	uint8_t CtrlMemtoReg;
	uint8_t CtrlALUOp;
	uint8_t CtrlMemWrite;
	uint8_t CtrlALUSrc;
	uint8_t CtrlRegWrite;
	uint8_t CtrlBranch;
	uint8_t CtrlALUOtherOp; // for instructions slt, slti, lw, sw . use last 3 bits of opcode
	uint8_t CtrlEcall;
	// values read from registers
	uint32_t rs1Value;
	uint32_t rs2Value;
	// generated signed immediate value
	int immediate;
	/* 
	   funct3 and funct7 from instruction bits[30, 14:12]
	   bit[30] = single bit funct7 code at bit 3 in funct3and7
	   bits[14:12] = 3 bit funct3 code at bits 2-0 in funct3and7
	*/ 
	uint8_t funct3and7;
	// destination register
	uint8_t rd;
	// branch target address
	uint32_t branchAddr;
	// program counter
	uint32_t id_ex_pc;
};

struct EX_MEM_buffer {
	// control singnals
	uint8_t CtrlMemRead;
	uint8_t CtrlMemtoReg;
	uint8_t CtrlMemWrite;
	uint8_t CtrlRegWrite;
	// ALU result
	uint32_t ALUResult;
	// values read from registers
	uint32_t rs2Value;
	// destination register
	uint8_t rd;
};

struct MEM_WB_buffer {
	// control singnals
	uint8_t CtrlMemtoReg;
	uint8_t CtrlRegWrite;
	// value read from memory address
	uint32_t ReadData;
	// ALU result
	uint32_t ALUResult;
	// destination register
	uint8_t rd;
};

//signals for the ALU controller
struct ALU_SIG {
	uint8_t CtrlMath; //000
	uint8_t CtrlShiftL; //001
	uint8_t CtrlOther; //010
	uint8_t CtrlJump; // 011
	uint8_t CtrlXOr; //100
	uint8_t CtrlShiftR; //101
	uint8_t CtrlOr; //110
	uint8_t CtrlAnd; //111
	uint8_t CtrlAui; //auipc
	uint8_t CtrlLui; //lui
	uint8_t CtrlMod; //funct7
};

// public functions
int fetch(struct ID_EX_buffer *in, struct IF_ID_buffer *out, struct cpu_context *cpu_ctx);
int decode(struct IF_ID_buffer *in, struct ID_EX_buffer *out, struct cpu_context *cpu_ctx);
int execute( struct ID_EX_buffer *in, struct EX_MEM_buffer *out, struct cpu_context *cpu_ctx);
int memory( struct EX_MEM_buffer *in, struct MEM_WB_buffer *out);
int writeback( struct MEM_WB_buffer *in, struct cpu_context *cpu_ctx );

extern struct instruction_count inst_count;

int hazardDetection(struct IF_ID_buffer *if_id, struct ID_EX_buffer *id_ex, struct EX_MEM_buffer *ex_mem, struct MEM_WB_buffer *mem_wb, struct instruction_count *inst_count);

// local functions
uint32_t Mux(uint8_t ctrlSignal, uint32_t muxIn0, uint32_t muxIn1);

uint32_t Adder(uint32_t value1, uint32_t value2);

int ControlLogic(uint8_t opcode, struct ID_EX_buffer* out);

int ReadRegister(uint8_t rs1, uint8_t rs2, struct ID_EX_buffer* out, struct cpu_context* cpu_ctx);

int ImmediateGeneration(uint32_t instruction, struct ID_EX_buffer* out);

int BranchLogic(struct IF_ID_buffer* in, struct ID_EX_buffer* out, struct cpu_context* cpu_ctx);

int ALUController(struct ID_EX_buffer* in, struct ALU_SIG* alu_sig);

int ALU(struct ID_EX_buffer* in, struct EX_MEM_buffer* out, struct ALU_SIG* alu_sig);

int EcallHandler(struct ID_EX_buffer* in, struct cpu_context* cpu_ctx);