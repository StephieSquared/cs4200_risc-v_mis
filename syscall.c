/*
 * Copyright (C) 2018 Gedare Bloom
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include "cpu.h"
#include "memory.h"

void sys_exit(int v, struct cpu_context *cpu_ctx) 
{
	printf("\nExiting RISC-V simulator... Code: %x\n", v);
	cpu_ctx->sys_exit = 1;
}

void sys_print_int(uint32_t value)
{
	printf("%d", value);
}

void sys_print_str(uint32_t strAddress)
{
	int stringIndex = (strAddress - 0x10000000) / 4;
	int stringPosition = (strAddress - 0x10000000) % 4;

	uint32_t data = data_memory[stringIndex];
	uint8_t character[4];

	for (int i = stringPosition; i < 4; i++)
	{
		character[i] = (data >> (i * 8)) & 0xFF;
	}

	while (character[stringPosition] != 0x0)
	{
		printf("%c", character[stringPosition]);
		
		stringPosition++;
		if (stringPosition == 4)
		{
			stringPosition = 0;
			stringIndex++;
			data = data_memory[stringIndex];
			for (int i = stringPosition; i < 4; i++)
			{
				character[i] = (data >> i) & 0xFF;
			}
		}
	}
}

