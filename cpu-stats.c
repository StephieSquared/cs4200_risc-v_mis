/*

*/

#include "cpu.h"
#include "cpu-stats.h"
#include "memory.h"
#include <stdint.h>

char* instructions[26] = { 
	// R-type
	"add",	// 0
	"and",	// 1
	"or",	// 2
	"slt",	// 3
	"sll",	// 4
	"sra",	// 5
	"srl",	// 6
	"sub",	// 7
	"xor",	// 8
	// I-type
	"addi",	// 9	
	"andi",	// 10
	"lw",	// 11
	"ori",	// 12
	"slti",	// 13
	"slli",	// 14
	"srai",	// 15
	"srli",	// 16
	"xori",	// 17
	// SB-type
	"beq",	// 18
	"bne",	// 19
	// S-type
	"sw",	// 20
	// UJ-type
	"jal",	// 21
	"jalr",	// 22
	// U-type
	"auipc",// 23
	"lui",	// 24
	// ecall
	"ecall"	// 25
};

int cpuStatCounterSingleCycle(struct instruction_count *inst_count, struct ID_EX_buffer *id_ex)
{
	// get instruction of execute stage PC from instruction memory
	int instIndex = (id_ex->id_ex_pc - 0x00400000) / 4;
	uint32_t instruction = 0;
	if (instIndex <= 1023)
	{
		instruction = instruction_memory[instIndex];
	}
	
	// dont count stats for empty 
	if (instruction == 0)
	{
		return 0;
	}
	else
	{
		// track executed instructions for pipeline
		inst_count->executedInstructions++;
	}

	uint8_t opcode = instruction & 0x7F;
	uint8_t funct3 = (instruction >> 12) & 0x7;
	uint8_t funct7 = (instruction >> 30) & 0x1;

	switch (opcode)
	{
		// R-type
	case 0x33:
		switch (funct3)
		{
			// add, sub
		case 0x0:
			if (funct7) // sub
			{
				inst_count->count[7]++;
			}
			else // add
			{
				inst_count->count[0]++;
			}
			break;
			// sll
		case 0x1:
			inst_count->count[4]++;
			break;
			// slt
		case 0x2:
			inst_count->count[3]++;
			break;
			// xor
		case 0x4:
			inst_count->count[8]++;
			break;
			// sra, srl
		case 0x5:
			if (funct7) // sra
			{
				inst_count->count[5]++;
			}
			else // srl
			{
				inst_count->count[6]++;
			}
			break;
			// or
		case 0x6:
			inst_count->count[2]++;
			break;
			// and
		case 0x7:
			inst_count->count[1]++;
			break;
		}
		break;
		// I-type
	case 0x13:
		switch (funct3)
		{
			// addi
		case 0x0:
			inst_count->count[9]++;
			break;
			// slli
		case 0x1:
			inst_count->count[14]++;
			break;
			// slti
		case 0x2:
			inst_count->count[13]++;
			break;
			// xori
		case 0x4:
			inst_count->count[17]++;
			break;
			// srai, srli
		case 0x5:
			if (funct7) // srai
			{
				inst_count->count[15]++;
			}
			else // srli
			{
				inst_count->count[16]++;
			}
			break;
			// ori
		case 0x6:
			inst_count->count[12]++;
			break;
			// andi
		case 0x7:
			inst_count->count[10]++;
			break;
		}
		break;
	case 0x3: // lw
		inst_count->count[11]++;
		break;
		// SB-type
	case 0x63:
		if (funct3) // bne
		{
			inst_count->count[19]++;
		}
		else // beq
		{
			inst_count->count[18]++;
		}
		break;
		// S-type
	case 0x23: // sw
		inst_count->count[20]++;
		break;
		// UJ-type
	case 0x6F: // jal
		inst_count->count[21]++;
		break;
	case 0x67: // jalr
		inst_count->count[22]++;
		break;
		// U-type
	case 0x17: // auipc
		inst_count->count[23]++;
		break;
	case 0x37: // lui
		inst_count->count[24]++;
		break;
		// ecall
	case 0x73:
		inst_count->count[25]++;
		break;
	}
	
	inst_count->totalCycles++;
	
}

int cpuStatCounterPipline(struct instruction_count* inst_count, struct ID_EX_buffer *id_ex)
{
	if (inst_count->totalCycles >= 2)
	{
		if (inst_count->currentStalled <= 0 && id_ex->id_ex_pc >= 0x00400000)
		{
			cpuStatCounterSingleCycle(inst_count, id_ex);
			
		}
		else
		{
			inst_count->totalCycles++;
		}
	}
	else
	{
		inst_count->totalCycles++;
	}

	inst_count->currentStalled--;
	
}