/*

*/

extern char* instructions[26];

// track number of times an instruction has been executed
struct instruction_count {
	char* name[26];
	int count[26];
	int totalCycles;
	// used in pipeline version only
	int currentStalled;
	int stalledCycles;
	int executedInstructions;
};

int cpuStatCounterSingleCycle(struct instruction_count *inst_count, struct ID_EX_buffer *id_ex);

int cpuStatCounterPipline(struct instruction_count *inst_count, struct ID_EX_buffer *id_ex);