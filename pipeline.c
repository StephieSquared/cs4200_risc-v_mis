/*
 * Copyright (C) 2018 Gedare Bloom
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "cpu.h"
#include "memory.h"
#include "syscall.h"
#include "cpu-stats.h"
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

//#define DEBUG

// prototypes
int SimExitConditionTest(struct cpu_context* cpu_ctx);
int PrintCpuStats(struct instruction_count* inst_count);

int main( int argc, char *argv[] )
{
	FILE *f;
	// initialize buffer values to 0
	struct IF_ID_buffer if_id = { 0 };
	struct ID_EX_buffer id_ex = { 0 };
	struct EX_MEM_buffer ex_mem = { 0 };
	struct MEM_WB_buffer mem_wb = { 0 };
	struct cpu_context cpu_ctx = { 0 };
	int i;
	size_t count;

	// initialize cpu stats
	struct instruction_count inst_count = { 0 };
	for (int i = 0; i < 26; i++)
	{
		inst_count.name[i] = instructions[i];
		inst_count.count[i] = 0;
	}

	/* Set the PC */
	cpu_ctx.PC = 0x00400000;
	if_id.pc = cpu_ctx.PC;
	if_id.next_pc = cpu_ctx.PC;

	/* Initialize register to 0 */
	for ( i = 0; i < 32; i++ ) {
		cpu_ctx.GPR[i] = 0;
	}

	/* Set the SP */
	cpu_ctx.GPR[2] = 0x10001000;

	/* Read memory from the input file */
	f = fopen(argv[1], "r");
	assert(f);
	count = fread(data_memory, sizeof(uint32_t), 1024, f);
	assert(count == 1024);
	count = fread(instruction_memory, sizeof(uint32_t), 1024, f);
	assert(count == 1024);
	fclose(f);

	/// <summary>
	/// start of cpu instruction cycle
	/// </summary>
	while(cpu_ctx.PC != 0) {


        // write to register
		writeback(&mem_wb, &cpu_ctx);

        // read and write to data_memory
		memory(&ex_mem, &mem_wb);
		
        // execute instruction. recieve cpu_ctx for PC
		execute(&id_ex, &ex_mem, &cpu_ctx);

		// track instruction count and cpu cycle
		cpuStatCounterPipline(&inst_count, &id_ex);
		
		// decode instruction. recieve cpu_ctx for Register access
		decode( &if_id, &id_ex, &cpu_ctx );

		// run hazard detection on decoded instruction
		hazardDetection(&if_id, &id_ex, &ex_mem, &mem_wb, &inst_count);

        // fetch instruction. recieve branch decision from decode stage ID_EX_buffer. recieve cpu_ctx for PC tracking.
		fetch(&id_ex, &if_id, &cpu_ctx);


	
		// test for simulation exit condition
		SimExitConditionTest(&cpu_ctx);

#if defined(DEBUG)
		if (cpu_ctx.PC != 0)
		{
			printf("FETCH from PC=%x\n", cpu_ctx.PC);
		}
#endif
		
	} // end cpu instruction cycle

	// print cpu stats
	PrintCpuStats(&inst_count);
	
	// exit program
	return 0;
}

/// <summary>
/// check for simulation exit conditions. if pc is 0. set pc = 0 if sys_exit was executed.
/// </summary>
/// <param name="cpu_ctx"></param>
/// <returns></returns>
int SimExitConditionTest(struct cpu_context *cpu_ctx)
{
	// print exit message if PC = 0
	if (cpu_ctx->PC == 0)
	{
		printf("\nExiting RISC-V simulator... Program Counter is 0\n");
	}

	// kill loop if sys_exit was executed by setting pc = 0
	if (cpu_ctx->sys_exit == 1)
	{
		cpu_ctx->PC = 0;
	}

	// kill loop 3 cycles after empty instruction is fetched
	if (cpu_ctx->spinDown > 0)
	{
		cpu_ctx->spinDown--;
		if (cpu_ctx->spinDown == 0)
		{
			printf("\nExiting RISC-V simulator... Reached end of program\n");
			cpu_ctx->PC = 0;
		}
	}
}

/// <summary>
/// print simulation cpu stats
/// </summary>
/// <param name="inst_count"></param>
/// <returns></returns>
int PrintCpuStats(struct instruction_count *inst_count)
{
	// print totals
	printf("\n\n\n");
	printf("--- RISC-V Simulation Statistics ---\n");
	printf("Executed instructions: %d\t\t", inst_count->executedInstructions);
	printf("Stalled cycles: %d\n", inst_count->stalledCycles);
	printf("Squashed instructions: 0\t\t");
	printf("Warmup cycles: 2\n");
	printf("Total cycles: %d\n", inst_count->totalCycles);
	printf("CPI: %.2f\n\n", ((float)inst_count->totalCycles / (float)inst_count->executedInstructions));
	// print individual instruction counts.
	// print R-type inisturction stats
	printf("---R-Type instructions---\n");
	printf("%s: %d\t\t%s: %d\t\t%s: %d\t\t%s: %d\t\t%s: %d\n"
		"%s: %d\t\t%s: %d\t\t%s: %d\t\t%s: %d\n\n",
		inst_count->name[0], inst_count->count[0], inst_count->name[1], inst_count->count[1],
		inst_count->name[2], inst_count->count[2], inst_count->name[3], inst_count->count[3],
		inst_count->name[4], inst_count->count[4], inst_count->name[5], inst_count->count[5],
		inst_count->name[6], inst_count->count[6], inst_count->name[7], inst_count->count[7],
		inst_count->name[8], inst_count->count[8]);
	// print I-type instruction stats
	printf("---I-Type instructions---\n");
	printf("%s: %d\t\t%s: %d\t\t%s: %d\t\t%s: %d\t\t%s: %d\n"
		"%s: %d\t\t%s: %d\t\t%s: %d\t\t%s: %d\n\n",
		inst_count->name[9], inst_count->count[9], inst_count->name[10], inst_count->count[10],
		inst_count->name[11], inst_count->count[11], inst_count->name[12], inst_count->count[12],
		inst_count->name[13], inst_count->count[13], inst_count->name[14], inst_count->count[14],
		inst_count->name[15], inst_count->count[15], inst_count->name[16], inst_count->count[16],
		inst_count->name[17], inst_count->count[17] );
	// print SB-type instruction stats
	printf("---SB-Type instructions---\n");
	printf("%s: %d\t\t%s: %d\n\n",
		inst_count->name[18], inst_count->count[18], inst_count->name[19], inst_count->count[19]);
	// print S-type instruction stats
	printf("---S-Type instructions---\n");
	printf("%s: %d\n\n", inst_count->name[20], inst_count->count[20]);
	// print UJ-type instruction stats
	printf("---UJ-Type instructions---\n");
	printf("%s: %d\t\t%s: %d\n\n",
		inst_count->name[21], inst_count->count[21], inst_count->name[22], inst_count->count[22]);
	// print U-type instruction stats
	printf("---U-Type instructions---\n");
	printf("%s: %d\t\t%s: %d\n\n",
		inst_count->name[23], inst_count->count[23], inst_count->name[24], inst_count->count[24]);
	// print ecall stats
	printf("---ecalls---\n");
	printf("%s: %d\n\n", inst_count->name[25], inst_count->count[25]);
}