/*
  *Copyright (C) 2018 Gedare Bloom
 *
  *Redistribution and use in source and binary forms, with or without
  *modification, are permitted provided that the following conditions
  *are met:
  *1. Redistributions of source code must retain the above copyright
  *   notice, this list of conditions and the following disclaimer.
  *2. Redistributions in binary form must reproduce the above copyright
  *   notice, this list of conditions and the following disclaimer in the
  *   documentation and/or other materials provided with the distribution.
 *
  *THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  *AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  *IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  *ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
  *LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  *CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  *SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  *INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  *CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  *ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  *POSSIBILITY OF SUCH DAMAGE.
 */

#include "cpu.h"
#include "memory.h"
#include "syscall.h"
#include "cpu-stats.h"

/// <summary>
/// fetches instruction at current PC
/// </summary>
int fetch( struct ID_EX_buffer *in, struct IF_ID_buffer *out, struct cpu_context *cpu_ctx )
{
	// set current PC based on branch decision from Decode stage
	out->pc = cpu_ctx->PC = Mux(in->CtrlBranch, out->next_pc, in->branchAddr);
	// set next PC = PC+4
	out->next_pc = Adder(out->pc, 4);
	// get instruction of PC from instruction memory
	int instIndex = (out->pc - 0x00400000) / 4;
	if (instIndex <= 1023)
	{
		out->instruction = instruction_memory[instIndex];
	}
	else
	{
		if (cpu_ctx->spinDown == 0)
		{
			cpu_ctx->spinDown = 3;
		}
	}

	if (out->instruction == 0)
	{
		if (cpu_ctx->spinDown == 0)
		{
			cpu_ctx->spinDown = 3;
		}
	}

	return 0;
}

/// <summary>
/// decode instruction. set control signals for instruction. preform branch logic.
/// </summary>
/// <param name="in"></param>
/// <param name="out"></param>
/// <param name="cpu_ctx"></param>
/// <returns></returns>
int decode( struct IF_ID_buffer *in, struct ID_EX_buffer *out, struct cpu_context *cpu_ctx )
{
	// get opcode, rs1, rs2, and rd from instruction
	uint8_t opcode = in->instruction & 0x7F;
	uint8_t rs1 = (in->instruction >> 15) & 0x1F;
	uint8_t rs2 = (in->instruction >> 20) & 0x1F;
	out->rd = (in->instruction >> 7) & 0x1F;
	// get funct 3 and 7 and package them into a 4 bit container: 
	// instruction[30,14:12] -> funct3and7[3:0] where bit 3 is the 1-bit funct7 code and bits 0-2 are the 3-bit funct3 code
	out->funct3and7 = ((in->instruction >> 27) & 0x8) + ((in->instruction >> 12) & 0x7);
	// pass instruction pc to next stage
	out->id_ex_pc = in->pc;

	// sets control signals based on opcode and stores them in ID/EX buffer 
	ControlLogic(opcode, out);
	// get value stored in register rs1 and rs2 and stores values in ID/EX buffer
	ReadRegister(rs1, rs2, out, cpu_ctx);
	// generate immediate value based on opcode and funct3 and stores them in ID/EX buffer
	ImmediateGeneration(in->instruction, out);
	// check for branch control signal and perform branch logic
	BranchLogic(in, out, cpu_ctx);

	return 0;
}

int execute( struct ID_EX_buffer *in, struct EX_MEM_buffer *out, struct cpu_context *cpu_ctx)
{
	struct ALU_SIG alu_sig = { 0 };
	out->rd = in->rd;
	out->CtrlMemRead = in->CtrlMemRead;
	out->CtrlMemtoReg = in->CtrlMemtoReg;
	out->CtrlMemWrite = in->CtrlMemWrite;
	out->CtrlRegWrite = in->CtrlRegWrite;
	out->rs2Value = in->rs2Value;

	ALUController(in, &alu_sig);
	ALU(in, out, &alu_sig);
	
	EcallHandler(in, cpu_ctx);

	return 0;
}

/// <summary>
/// check if data needs to be written to data memory or if data needs to be read from data memory. then performs 
/// read or write. 
/// </summary>
/// <param name="in"></param>
/// <param name="out"></param>
/// <param name="cpu_ctx"></param>
/// <returns></returns>
int memory( struct EX_MEM_buffer *in, struct MEM_WB_buffer *out)
{
	// pass control signals to next stage
	out->CtrlMemtoReg = in->CtrlMemtoReg;
	out->CtrlRegWrite = in->CtrlRegWrite;
	out->ALUResult = in->ALUResult;
	out->rd = in->rd;
	out->ReadData = in->ALUResult;

	// lw instruction. read data from memory at the address from ALUresult
	if (in->CtrlMemRead)
	{
		int memoryIndex = (in->ALUResult - 0x10000000) / 4;
		out->ReadData = data_memory[memoryIndex];
	}
	// sw instruction. write data to memory at the address from ALUresult
	else if (in->CtrlMemWrite)
	{
		int memoryIndex = (in->ALUResult - 0x10000000) / 4;
		data_memory[memoryIndex] = in->rs2Value;
	}

	return 0;
}

/// <summary>
/// check if data needs to be written to the GPR. then checks if ALU results or data read from memory are
/// being written to the rd register.
/// </summary>
/// <param name="in"></param>
/// <param name="cpu_ctx"></param>
/// <returns></returns>
int writeback( struct MEM_WB_buffer *in, struct cpu_context *cpu_ctx )
{
	if (in->CtrlRegWrite && in->rd)
	{
		cpu_ctx->GPR[in->rd] = Mux(in->CtrlMemtoReg, in->ALUResult, in->ReadData);
	}

	return 0;
}


int hazardDetection(struct IF_ID_buffer *if_id, struct ID_EX_buffer *id_ex, struct EX_MEM_buffer *ex_mem, struct MEM_WB_buffer *mem_wb, struct instruction_count *inst_count)
{
	uint8_t opcode = if_id->instruction & 0x7F;
	uint8_t rs1 = (if_id->instruction >> 15) & 0x1F;
	uint8_t rs2 = (if_id->instruction >> 20) & 0x1F;
	// track if current instruction uses rs1 and rs2
	int checkRS1 = 0;
	int checkRS2 = 0;
	
	// check if previous 2 instructions need to write to register
	if (ex_mem->CtrlRegWrite || mem_wb->CtrlRegWrite) 
	{
		// set if instruction uses rs1 and/or rs2
		switch (opcode)
		{
			// R-type
		case 0x33:
			// SB-type
		case 0x63:
			// S-type
		case 0x23:
			checkRS1 = 1;
			checkRS2 = 1;
			break;
			// I-type
		case 0x13:
			// lw instruction
		case 0x3:
			// jalr instruction
		case 0x67:
			checkRS1 = 1;
			break;
		// ecalll
		case 0x73:
			// test for register a0 or a7 before the ecall 
			if (ex_mem->rd == 10 || ex_mem->rd == 17 || mem_wb->rd == 10 || mem_wb->rd == 17)
			{
				// stall next fetch and decode instruction and squash next execute instructions
				struct ID_EX_buffer emptyID_EX = { 0 };
				*id_ex = emptyID_EX;
				if_id->next_pc = if_id->pc;
				// track stall and squash stats
				inst_count->stalledCycles++;
				inst_count->currentStalled = 1;
				if (ex_mem->rd == 10 || ex_mem->rd == 17)
				{
					inst_count->currentStalled++;
				}
			}
			break;
		}

		// check if instruction uses rs1 or rs2
		if (checkRS1)
		{
			// check if rs1 or rs2 == rd from previous 2 instructions
			if ((rs1 == ex_mem->rd && ex_mem->rd != 0) || (rs1 == mem_wb->rd && mem_wb->rd != 0))
			{
				// stall next fetch and decode instruction and squash next execute instructions
				struct ID_EX_buffer emptyID_EX = { 0 };
				*id_ex = emptyID_EX;
				if_id->next_pc = if_id->pc;
				// track stall and squash stats
				inst_count->stalledCycles++;
				inst_count->currentStalled = 1;
				if (rs1 == ex_mem->rd && ex_mem->rd != 0)
				{
					inst_count->currentStalled++;
				}
			}
		}

		if (checkRS2)
		{
			// check if rs1 or rs2 == rd from previous 2 instructions
			if ((rs2 == ex_mem->rd && ex_mem->rd != 0) || (rs2 == mem_wb->rd && mem_wb->rd != 0))
			{
				// stall next fetch and decode instruction and squash next execute instructions
				struct ID_EX_buffer emptyID_EX = { 0 };
				*id_ex = emptyID_EX;
				if_id->next_pc = if_id->pc;
				// track stall and squash stats
				
				if (inst_count->currentStalled <= 0) // don't double count stats if stalled from rs1 
				{
					inst_count->stalledCycles++;
					inst_count->currentStalled = 1;
				}
				if (rs2 == ex_mem->rd && ex_mem->rd != 0 && inst_count->currentStalled != 2)
				{
					inst_count->currentStalled++;
					inst_count->stalledCycles++;
				}
			}
		}

	}
}

/// helper functions ///

/// <summary>
/// mux recieves a control signal and 2 inputs to select between. if control signal is 0
/// select muxIn0. if control signal is 1 select muxIn1.
/// </summary>
uint32_t Mux(uint8_t ctrlSignal, uint32_t muxIn0, uint32_t muxIn1)
{
	if (ctrlSignal)
	{
		return muxIn1;
	}
	else
	{
		return muxIn0;
	}
}

/// <summary>
/// adds 2 values and returns result
/// </summary>
uint32_t Adder(uint32_t value1, uint32_t value2)
{
	return value1 + value2;
}

/// <summary>
/// sets control signals based on opcode and stores them in ID/EX buffer 
/// </summary>
/// <param name="opcode"></param>
/// <param name="out"></param>
/// <returns></returns>
int ControlLogic(uint8_t opcode, struct ID_EX_buffer *out)
{
	// 0 control singnals
	out->CtrlALUOp = 0;
	out->CtrlRegWrite = 0;
	out->CtrlMemRead = 0;
	out->CtrlMemtoReg = 0;
	out->CtrlMemWrite = 0;
	out->CtrlALUSrc = 0;
	out->CtrlBranch = 0;
	out->CtrlALUOtherOp = 0;
	out->CtrlEcall = 0;

	switch (opcode)
	{
	// R-type // instructions add, and, or, slt, sll, sra, srl, sub, xor
	case 0x33:
		// control singnals
		out->CtrlALUOp = 1;
		out->CtrlRegWrite = 1;
		out->CtrlALUOtherOp = ((opcode >> 4) & 0x7);
		break;
	// I-type // instructions addi, andi, ori, slti, xori
	case 0x13:
		// control singnals
		out->CtrlALUOp = 1;
		out->CtrlALUSrc = 1;
		out->CtrlRegWrite = 1;
		out->CtrlALUOtherOp = ((opcode >> 4) & 0x7);
		break;
	// U-type
	case 0x37: // lui instruction
	case 0x17: // auipc instruction
		out->CtrlALUSrc = 1;
		out->CtrlRegWrite = 1;
		out->CtrlALUOtherOp = ((opcode >> 4) & 0x7);
		break;
	// lw instruction
	case 0x3:
		// control singnals
		out->CtrlMemRead = 1;
		out->CtrlMemtoReg = 1;
		out->CtrlALUOp = 1;
		out->CtrlALUSrc = 1;
		out->CtrlRegWrite = 1;
		out->CtrlALUOtherOp = ((opcode >> 4) & 0x7);
		break;
	// SB-type // instructions beq, bne
	case 0x63:
		// control singnals
		out->CtrlBranch = 1;
		break;
	// S-type
	case 0x23: // sw instruction
		// control singnals
		out->CtrlALUOp = 1;
		out->CtrlMemWrite = 1;
		out->CtrlALUSrc = 1;
		out->CtrlALUOtherOp = ((opcode >> 4) & 0x7);
		break;
	// UJ-type
	case 0x6F: // jal
	case 0x67: // jalr
		// control singnals
		out->CtrlALUOtherOp = ((opcode >> 4) & 0x7);
		out->CtrlRegWrite = 1;
		out->CtrlBranch = 1;
		break;
	// ecall
	case 0x73:
		out->CtrlEcall = 1;
		break;
	}

	return 0;
}

/// <summary>
/// get value stored in register rs1 and rs2 and stores values in ID/EX buffer
/// </summary>
/// <param name="rs1"></param>
/// <param name="rs2"></param>
/// <param name="out"></param>
/// <param name="cpu_ctx"></param>
/// <returns></returns>
int ReadRegister(uint8_t rs1, uint8_t rs2, struct ID_EX_buffer *out, struct cpu_context *cpu_ctx)
{
	out->rs1Value = cpu_ctx->GPR[rs1];
	out->rs2Value = cpu_ctx->GPR[rs2];

	return 0;
}

/// <summary>
/// generates immediate value based on opcode and funct3 and stores them in ID/EX buffer
/// </summary>
/// <param name="instruction"></param>
/// <param name="out"></param>
/// <returns></returns>
int ImmediateGeneration(uint32_t instruction, struct ID_EX_buffer *out)
{
	// get opcode and funct3
	uint8_t opcode = instruction & 0x7F;
	uint8_t funct3 = (instruction >> 12) & 0x7;

	switch (opcode)
	{
	// I-type // instruction addi, andi, ori, slti, xori
	case 0x13:
	// UJ-type jalr
	case 0x67:
	// I-type lw
	case 0x3:
		// I-type shifts instructions slli, srai, srli
		if (funct3 == 1 || funct3 == 5)
		{
			out->immediate = (uint32_t)((signed int)instruction >> 20);
		}
		// all other I-type and UJ-Type jalr 
		else
		{
			out->immediate = (uint32_t)((signed int)instruction >> 20);
		}
		break;
	// SB-type // instructions beq, bne
	case 0x63:
		out->immediate = ((instruction >> 19) & 0x1000) + ((instruction >> 20) & 0xFE0) + ((instruction >> 7) & 0x1E) + ((instruction << 4) & 0x800);
		break;
	// S-type // instruction sw
	case 0x23:
		out->immediate = ((instruction >> 20) & 0xFE0) + ((instruction >> 7) & 0x1F);
		break;
	// UJ-type jal
	case 0x6F:
		out->immediate = ((((instruction >> 11) & 0x100000) + ((instruction >> 21) & 0x7FE) + ((instruction >> 9) & 0x800) + (instruction & 0xFF000)) << 1);
		if (((instruction & 0x80000000) >> 31) == 1) out->immediate += 0xFFC00000;
		break;
	// U-type // instruction auipc, lui
	case 0x17:
	case 0x37:
		out->immediate = instruction & 0xFFFFF000;
		break;
	}

	return 0;
}

/// <summary>
/// checks for branch control signal. if signal = 1, finds branch PC address. then checks for conditional branches and preforms condition check
/// </summary>
/// <param name="in"></param>
/// <param name="out"></param>
/// <param name="cpu_ctx"></param>
/// <returns></returns>
int BranchLogic(struct IF_ID_buffer *in, struct ID_EX_buffer *out, struct cpu_context *cpu_ctx)
{
	uint8_t opcode = in->instruction & 0x7F;
	uint8_t funct3 = (in->instruction >> 12) & 0x7;

	// if branch control signal = 1 preform branch decision logic
	if (out->CtrlBranch == 1)
	{
		// add immediate value of relitive address to PC and store branch PC address in ID/EX buffer
		if (out->immediate >> 31 != 0)
		{
			out->branchAddr = in->pc - (~out->immediate + 1);
		}
		else
		{
			out->branchAddr = in->pc + out->immediate;
		}
		

		// test for conditional jump ( beq, bne) then test for condition
		if (opcode == 0x63)
		{
			// beq
			if (funct3 == 0)
			{
				// if rs1 != rs2 set branch control signal = 0
				if (out->rs1Value != out->rs2Value)
				{
					out->CtrlBranch = 0;
				}
			}
			// bne
			else
			{
				// if rs1 = rs2 set branch control signal = 0
				if (out->rs1Value == out->rs2Value)
				{
					out->CtrlBranch = 0;
				}
			}
		}
		// jalr
		else if (opcode == 0x67)
		{
			out->branchAddr = out->rs1Value + out->immediate;
		}
	}

	return 0;
}

//ALU controller function
int ALUController(struct ID_EX_buffer *in, struct ALU_SIG *alu_sig) {
	//if we need to even do anything
	int funct3 = in->funct3and7 & 7;
	int funct7 = (in->funct3and7 >> 3) & 1; //1 or 0
	alu_sig->CtrlMod = funct7;

	if (in->CtrlALUOp == 1) {
		switch(funct3){
			case 0x0:
			//add, addi, sub
				alu_sig->CtrlMath = 1;
				break;
			case 0x1:
			//left shift
				alu_sig->CtrlShiftL = 1;
				break;
			case 0x2:
			// slt, slti, lw, sw
				alu_sig->CtrlOther = 1;
				break;
			// no 0x3
			case 0x4:
			//Xor
				alu_sig->CtrlXOr = 1;
				break;
			case 0x5:
			//right shift
				alu_sig->CtrlShiftR = 1;
				break;
			case 0x6:
			//or
				alu_sig->CtrlOr = 1;
				break;
			case 0x7:
			//and
				alu_sig->CtrlAnd = 1;
				break;	
		} //end switch

	}
	// auipc
	else if (in->CtrlALUOtherOp == 0x1)
	{
			alu_sig->CtrlAui = 1;
	}
	// lui
	else if (in->CtrlALUOtherOp == 0x3)
	{
		alu_sig->CtrlLui = 1;
	}
	// jal, jalr
	else if (in->CtrlALUOtherOp == 0x6)
	{
		alu_sig->CtrlJump = 1;
	}
	return 0;
} //end ALUController

int ALU( struct ID_EX_buffer *in, struct EX_MEM_buffer *out, struct ALU_SIG *alu_sig) {
	//in has values, out holds result, cpu_ctx has PC for auipc

	//add, addi, sub
	if(alu_sig->CtrlMath == 1) {

		if (in->CtrlALUSrc) // addi
		{
			out->ALUResult = in->rs1Value + in->immediate;
		}
		else if (alu_sig->CtrlMod) // sub
		{
			out->ALUResult = in->rs1Value - in->rs2Value;
		}
		else // add
		{
			out->ALUResult = in->rs1Value + in->rs2Value;
		}
	} //end math		

	//sll, sli
	else if (alu_sig->CtrlShiftL == 1) {
		//test for imm
		if(in->CtrlALUSrc == 0) {
			out->ALUResult = in->rs1Value << in->rs2Value;
		}
		else {
			out->ALUResult = in->rs1Value << in->immediate;
		}
	} //end left shift
	
	//xor, xori
	else if (alu_sig->CtrlXOr == 1) {
		//test for imm
		if(in->CtrlALUSrc == 0) {
			out->ALUResult = in->rs1Value ^ in->rs2Value;
		}
		else {
			out->ALUResult = in->rs1Value ^ in->immediate;
		}
	} //end XOr
	
	//sra, srl, srai, srli
	else if (alu_sig->CtrlShiftR == 1) {		
		if(alu_sig->CtrlMod==0){ //srl, srli
			if(in->CtrlALUSrc==0){
				out->ALUResult = in->rs1Value >> in->rs2Value;
			}
			else{
				//imm = val2
				out->ALUResult = in->rs1Value >> in->immediate;
			}
		}
		else{ //sra, srai
			if(in->CtrlALUSrc==0){
				out->ALUResult = (uint32_t)(((signed int) in->rs1Value) >> in->rs2Value);
			}
			else{
				out->ALUResult = (uint32_t)(((signed int) in->rs1Value) >> in->immediate);
			}
		}
	} //end right shift
	
	//or, ori
	else if (alu_sig->CtrlOr == 1) {
		//test for imm
		if(in->CtrlALUSrc == 0) {
			out->ALUResult = in->rs1Value | in->rs2Value;
		}
		else{
			out->ALUResult = in->rs1Value | in->immediate;
		}
	} //end Or
	
	//and, andi
	else if (alu_sig->CtrlAnd == 1) {
		//test for imm
		if(in->CtrlALUSrc == 0) {
			out->ALUResult = in->rs1Value & in->rs2Value;
		}
		else {
			out->ALUResult = in->rs1Value & in->immediate;
		}
	} //end And
	
	//auipc special case
	else if (alu_sig->CtrlAui == 1) {
		out->ALUResult = in->id_ex_pc + in->immediate;
	} //end auipc special case

	//slt, slti, lw, sw
	else if (alu_sig->CtrlOther == 1) {

		switch (in->CtrlALUOtherOp)
		{
		case 0: // lw
			out->ALUResult = in->rs1Value + in->immediate;
			break;
		case 1: // slti
			if (in->rs1Value < in->immediate)
			{
				out->ALUResult = 1;
			}
			else
			{
				out->ALUResult = 0;
			}
			break;
		case 2: // sw
			out->ALUResult = in->rs1Value + in->immediate;
			out->rd = in->rs1Value;
			break;
		case 3: // slt
			if (in->rs1Value < in->rs2Value)
			{
				out->ALUResult = 1;
			}
			else
			{
				out->ALUResult = 0;
			}
			break;
		}
	} //end Other
	
	// jal, jalr
	else if (alu_sig->CtrlJump == 1)
	{
		out->ALUResult = in->id_ex_pc + 4;
	}
	// lui
	else if (alu_sig->CtrlLui == 1)
	{
		out->ALUResult = in->immediate;
	}
	return 0;
}

int EcallHandler(struct ID_EX_buffer *in, struct cpu_context *cpu_ctx)
{
	if (in->CtrlEcall)
	{
		switch (cpu_ctx->GPR[17]) // reg a7
		{
		case 1: // sys_print_int
			sys_print_int(cpu_ctx->GPR[10]);
			break;
		case 4: // sys_print_str
			sys_print_str(cpu_ctx->GPR[10]);
			break;
		case 10: // sys_exit
			sys_exit(cpu_ctx->GPR[10], cpu_ctx);
			break;
		}


	}
}