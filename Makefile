# simple makefile

all: single-cycle-mis pipeline-mis

single-cycle-mis: cpu.c cpu.h cpu-stats.c cpu-stats.h memory.c memory.h syscall.c syscall.h single-cycle.c 
	gcc -O2 cpu.c cpu-stats.c memory.c syscall.c single-cycle.c -o single-cycle-mis

pipeline-mis: cpu.c cpu.h cpu-stats.c cpu-stats.h memory.c memory.h syscall.c syscall.h pipeline.c 
	gcc -O2 cpu.c cpu-stats.c memory.c syscall.c pipeline.c -o pipeline-mis

clean:
	rm single-cycle-mis
	rm pipeline-mis
